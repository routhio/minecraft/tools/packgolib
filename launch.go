/* vim:set ts=4 sw=4 noet nowrap syntax=go ff=unix:
 *
 * Copyright 2021 Mark Crewson <mark@crewson.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package packgolib

import (
	"fmt"
	"github.com/buger/jsonparser"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/routhio/minecraft/tools/packgolib/minecraft"
	"gitlab.com/routhio/minecraft/tools/packgolib/modloader"
)

var JAVA_ARGUMENTS = []string{"-XX:+UseG1GC",
	"-XX:+UnlockExperimentalVMOptions",
	"-XX:G1NewSizePercent=20",
	"-XX:G1ReservePercent=20",
	"-XX:MaxGCPauseMillis=50",
	"-XX:G1HeapRegionSize=32M",
	"-Dsun.rmi.dgc.server.gcInterval=2147483646",
	"-Dfml.readTimeout=180"}

func Launch(m *minecraft.Minecraft, ml modloader.ModLoader, clientlocation string, playerName string, playerUuid string, playerToken string) error {
	fmt.Println("\n\n+--------------+\n|   LAUNCH!!   |\n+--------------+\n")
	launchArgs, err := m.GetLaunchArguments()
	if err != nil {
		return err
	}
	mainClass, err := m.GetMainClass()
	if err != nil {
		return err
	}

	if ml != nil {
		loaderLaunchArgs, err := ml.GetLaunchArguments()
		if err != nil {
			return err
		}
		if loaderLaunchArgs != "" {
			if strings.HasPrefix(loaderLaunchArgs, "--username ${auth_player_name}") {
				launchArgs = loaderLaunchArgs
			} else {
				launchArgs = launchArgs + " " + loaderLaunchArgs
			}
		}

		mainClass, err = ml.GetMainclass()
	}

	manifest, err := m.GetManifest()
	if err != nil {
		return err
	}
	id, err := jsonparser.GetString(manifest, "id")
	if err != nil {
		return err
	}
	assets, err := jsonparser.GetString(manifest, "assets")
	if err != nil {
		return err
	}
	versionType, err := jsonparser.GetString(manifest, "type")
	if err != nil {
		return err
	}

	clientjar := filepath.Join("libraries", "com", "mojang", "minecraft", id,
		fmt.Sprintf("minecraft-%s-client.jar", id))

	var cplist []string
	cplist = append(cplist, clientjar)

	libraries := make(map[string]string)

	libs, err := m.GetLibraries()
	if err != nil {
		return err
	}
	for _, lib := range libs {
		if !lib.MustExtract() {
			libraries[lib.GetArtifact()] = filepath.Join("libraries", lib.GetPathLocal())
		}
	}

	if ml != nil {
		libs, err := ml.GetClientLibraries()
		if err != nil {
			return err
		}
		for _, lib := range libs {
			libraries[lib.GetArtifact()] = filepath.Join("libraries", lib.GetPathLocal())
		}
	}

	for _, val := range libraries {
		cplist = append(cplist, val)
	}
	classpath := strings.Join(cplist, ":")

	launchArgs = strings.ReplaceAll(launchArgs, "${auth_player_name}", playerName)
	launchArgs = strings.ReplaceAll(launchArgs, "${version_name}", id)
	launchArgs = strings.ReplaceAll(launchArgs, "${game_directory}", ".")
	launchArgs = strings.ReplaceAll(launchArgs, "${assets_root}", "assets")
	launchArgs = strings.ReplaceAll(launchArgs, "${assets_index_name}", assets)
	launchArgs = strings.ReplaceAll(launchArgs, "${auth_uuid}", playerUuid)
	launchArgs = strings.ReplaceAll(launchArgs, "${auth_access_token}", playerToken)
	launchArgs = strings.ReplaceAll(launchArgs, "${auth_session}", playerToken)
	launchArgs = strings.ReplaceAll(launchArgs, "${user_type}", "mojang")
	launchArgs = strings.ReplaceAll(launchArgs, "${user_properties}", "{}")
	launchArgs = strings.ReplaceAll(launchArgs, "${version_type}", versionType)

	var args = []string{"-cp", classpath, "-Xms8192m", "-Xmx8192m"}
	args = append(args, JAVA_ARGUMENTS...)
	args = append(args, "-Djava.library.path=natives")
	args = append(args, mainClass)
	args = append(args, strings.Split(launchArgs, " ")...)

	err = os.Chdir(clientlocation)
	if err != nil {
		return err
	}
	cmd := exec.Command("/usr/bin/java", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	return err
}
