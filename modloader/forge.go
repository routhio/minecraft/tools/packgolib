/* vim:set ts=4 sw=4 noet nowrap syntax=go ff=unix:
 *
 * Copyright 2021 Mark Crewson <mark@crewson.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package modloader

import (
	"fmt"
	"github.com/buger/jsonparser"
	"github.com/go-resty/resty/v2"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/routhio/minecraft/tools/packgolib/minecraft"
	"gitlab.com/routhio/minecraft/tools/packgolib/util"
)

const FORGE_REPO_URL = "https://files.minecraftforge.net/maven/"
const CURSE_MODLOADER_INFO_URL = "https://addons-ecs.forgesvc.net/api/v2/minecraft/modloader"

var INCOMPATIBLE_FORGE_VERSION = []string{"14.23.5.2851"}

//

type ForgeModLoader struct {
	minecraftVersion string
	loaderVersion    string
	fullVersion      string
	modloaderInfo    []byte
	installProfile   []byte
	versionInfo      []byte

	downloader *util.Downloader
	client     *resty.Client
}

func NewForgeModLoader(minecraftVersion string, loaderVersion string, downloader *util.Downloader) *ForgeModLoader {
	f := &ForgeModLoader{
		minecraftVersion: minecraftVersion,
		loaderVersion:    loaderVersion,
		downloader:       downloader,
		client:           resty.New(),
	}
	f.calcFullVersion()
	return f
}

// ModLoader interface

func (f *ForgeModLoader) GetLaunchArguments() (string, error) {
	versionInfo, err := f.getVersionInfo()
	if err != nil {
		return "", err
	}

	var args []string
	_, err = jsonparser.ArrayEach(versionInfo, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		args = append(args, string(value))
	}, "arguments", "game")
	if err == nil {
		return strings.Join(args, " "), nil
	}
	return jsonparser.GetString(versionInfo, "minecraftArguments")
}

func (f *ForgeModLoader) GetClientLibraries() ([]*util.Library, error) {
	return f.getLibraries("client")
}

func (f *ForgeModLoader) GetServerLibraries() ([]*util.Library, error) {
	return f.getLibraries("server")
}

func (f *ForgeModLoader) GetMainclass() (string, error) {
	versionInfo, err := f.getVersionInfo()
	if err != nil {
		return "", err
	}
	return jsonparser.GetString(versionInfo, "mainClass")
}

func (f *ForgeModLoader) InstallClient(installLocation string, downloader *util.Downloader) error {
	fmt.Println("Downloading forge ...")
	libLocation := filepath.Join(installLocation, "libraries")
	libs, err := f.getLibraries("client")
	if err != nil {
		return err
	}
	installerLibs, err := f.getInstallerLibraries()
	if err != nil {
		return err
	}

	fmt.Println("Downloading forge libraries ...")
	sub := downloader.NewSubdownloader("libraries")
	err = InstallModLoaderLibraries(libs, sub, libLocation)
	if err != nil {
		return err
	}
	err = InstallModLoaderLibraries(installerLibs, sub, libLocation)
	if err != nil {
		return err
	}
	return f.runForgeProcessors(libLocation, "client")
}

func (f *ForgeModLoader) InstallServer(installLocation string, downloader *util.Downloader) error {
	return fmt.Errorf("not implemented yet")
}

//

func (f *ForgeModLoader) calcFullVersion() {
	mver, _ := util.MinecraftVersionAsInteger(f.minecraftVersion)
	if mver < 10900 {
		f.fullVersion = fmt.Sprintf("%s-%s-%s", f.minecraftVersion, f.loaderVersion, f.minecraftVersion)
	} else {
		f.fullVersion = fmt.Sprintf("%s-%s", f.minecraftVersion, f.loaderVersion)
	}
}

func (f *ForgeModLoader) getForgeInfoFromCurse() ([]byte, error) {
	if f.modloaderInfo == nil {
		modloaderInfoUrl := fmt.Sprintf("%s/forge-%s", CURSE_MODLOADER_INFO_URL, f.loaderVersion)
		resp, err := f.client.R().Get(modloaderInfoUrl)
		if err != nil {
			return nil, err
		}
		f.modloaderInfo = resp.Body()
	}
	return f.modloaderInfo, nil
}

func (f *ForgeModLoader) getVersionInfo() ([]byte, error) {
	if f.versionInfo == nil {
		modloaderInfo, err := f.getForgeInfoFromCurse()
		if err != nil {
			return nil, err
		}
		value, _, _, err := jsonparser.Get(modloaderInfo, "versionJson")
		if err != nil {
			return nil, err
		}
		if string(value) != "null" {
			unquoted := strings.ReplaceAll(string(value), "\\r\\n", "")
			unquoted = strings.ReplaceAll(unquoted, "\\\"", "\"")
			f.versionInfo = []byte(unquoted)
		}
	}

	// if that doesn't work, download the installer jarfile and extract the profile
	// from there ...
	if f.versionInfo == nil {
		installerFilename, err := f.getInstallerJar()
		if err != nil {
			return nil, err
		}
		f.versionInfo, err = util.ReadFileFromZip(installerFilename, "version.json")
		if err != nil {
			installProfile, err := f.getInstallProfile()
			if err != nil {
				return nil, err
			}
			value, _, _, err := jsonparser.Get(installProfile, "versionInfo")
			if err != nil {
				return nil, err
			}
			f.versionInfo = value
		}
	}
	return f.versionInfo, nil
}

func (f *ForgeModLoader) getInstallProfile() ([]byte, error) {
	if f.installProfile == nil {
		modloaderInfo, err := f.getForgeInfoFromCurse()
		if err != nil {
			return nil, err
		}
		value, _, _, err := jsonparser.Get(modloaderInfo, "installProfileJson")
		if err != nil {
			return nil, err
		}
		if string(value) != "null" {
			unquoted := strings.ReplaceAll(string(value), "\\r\\n", "")
			unquoted = strings.ReplaceAll(unquoted, "\\\"", "\"")
			f.installProfile = []byte(unquoted)
		}
	}

	// if that doesn't work, download the installer jarfile and extract the profile
	// from there ...
	if f.installProfile == nil {
		installerFilename, err := f.getInstallerJar()
		if err != nil {
			return nil, err
		}
		f.installProfile, err = util.ReadFileFromZip(installerFilename, "install_profile.json")
		if err != nil {
			return nil, err
		}
	}
	return f.installProfile, nil
}

func (f *ForgeModLoader) getInstallerJar() (string, error) {
	url := fmt.Sprintf("%snet/minecraftforge/forge/%s/forge-%s-installer.jar", FORGE_REPO_URL, f.fullVersion, f.fullVersion)
	sub := f.downloader.NewSubdownloader("forge")
	return sub.DownloadToDefaultFilename(url)
}

func (f *ForgeModLoader) getLibraries(side string) ([]*util.Library, error) {
	versionInfo, err := f.getVersionInfo()
	if err != nil {
		return nil, err
	}

	var dependents []*util.Library
	_, err = jsonparser.ArrayEach(versionInfo, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		library, _ := util.NewLibrary(value)
		if library.Matches() {
			dependents = append(dependents, library)
		}
	}, "libraries")
	if err != nil {
		return nil, err
	}
	return dependents, nil
}

func (f *ForgeModLoader) getInstallerLibraries() ([]*util.Library, error) {
	installProfile, err := f.getInstallProfile()
	if err != nil {
		return nil, err
	}

	var dependents []*util.Library
	jsonparser.ArrayEach(installProfile, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		library, _ := util.NewLibrary(value)
		if library.Matches() {
			dependents = append(dependents, library)
		}
	}, "libraries")
	/*if err != nil {
	    return nil, err
	} */
	return dependents, nil
}

func (f *ForgeModLoader) runForgeProcessors(libLocation string, side string) error {
	installProfile, err := f.getInstallProfile()
	if err != nil {
		return err
	}
	_, _, _, err = jsonparser.Get(installProfile, "processors")
	if err != nil {
		// no processors. we're done here
		return nil
	}

	data := make(map[string]string)
	err = jsonparser.ObjectEach(installProfile, func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
		datakey := string(key)
		dataval, err := jsonparser.GetString(value, side)
		if err != nil {
			return err
		}

		if strings.HasPrefix(dataval, "[") && strings.HasSuffix(dataval, "]") {
			data[datakey] = filepath.Join(libLocation, mavenToPath(dataval[1:len(dataval)-1]))
		} else if strings.HasPrefix(dataval, "'") && strings.HasSuffix(dataval, "'") {
			data[datakey] = dataval[1 : len(dataval)-1]
		} else {
			dataLocation := filepath.Join(libLocation, "data")
			if err := os.MkdirAll(dataLocation, 0777); err != nil {
				return err
			}
			installerFilename, err := f.getInstallerJar()
			if err != nil {
				return err
			}
			err = util.ExtractFileFromZip(installerFilename, strings.TrimLeft(dataval, "/"), filepath.Join(dataLocation, filepath.Base(dataval)))
			if err != nil {
				return err
			}
			data[datakey] = filepath.Join(dataLocation, filepath.Base(dataval))
		}
		return nil
	}, "data")
	if err != nil {
		return err
	}

	minecraftId, err := minecraft.NewMinecraft(f.minecraftVersion).GetId()
	if err != nil {
		return err
	}

	data["SIDE"] = side
	data["MINECRAFT_JAR"] = filepath.Join(libLocation, "com", "mojang", "minecraft", minecraftId, fmt.Sprintf("minecraft-%s-%s.jar", minecraftId, side))

	procNum := 0
	_, err = jsonparser.ArrayEach(installProfile, func(processor []byte, dataType jsonparser.ValueType, offset int, err error) {
		procNum = procNum + 1
		fmt.Printf("Running forge processor #%d ...\n", procNum)

		var args []string
		jsonparser.ArrayEach(processor, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
			arg := string(value)
			if strings.HasPrefix(arg, "{") && strings.HasSuffix(arg, "}") {
				arg = data[arg[1:len(arg)-1]]
			} else if strings.HasPrefix(arg, "[") && strings.HasSuffix(arg, "]") {
				arg = filepath.Join(libLocation, mavenToPath(arg[1:len(arg)-1]))
			}
			args = append(args, arg)
		}, "args")

		jar, err := jsonparser.GetString(processor, "jar")
		if err != nil {
			return
		}
		jarFile := filepath.Join(libLocation, mavenToPath(jar))
		var cplist = []string{jarFile}
		jsonparser.ArrayEach(processor, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
			cplist = append(cplist, filepath.Join(libLocation, mavenToPath(string(value))))
		}, "classpath")
		classpath := strings.Join(cplist, ":")

		mainClass, err := findMainclassInJar(jarFile)
		if err != nil {
			return
		}

		var cmdlineArgs = []string{"-cp", classpath, mainClass}
		cmdlineArgs = append(cmdlineArgs, args...)

		cmd := exec.Command("/usr/bin/java", cmdlineArgs...)
		//cmd.Stdout = os.Stdout
		//cmd.Stderr = os.Stderr
		err = cmd.Run()
	}, "processors")
	if err != nil {
		return err
	}
	return nil
}

//

func mavenToPath(name string) string {
	parts := strings.Split(name, ":")
	filename := parts[2]
	if len(parts) >= 4 {
		filename = fmt.Sprintf("%s-%s", parts[2], parts[3])
	}
	if strings.Contains(filename, "@") {
		filename = strings.ReplaceAll(filename, "@", ".")
	} else {
		filename = filename + ".jar"
	}

	initpath := strings.Split(parts[0], ".")
	initpath = append(initpath, parts[1])
	initpath = append(initpath, strings.Split(parts[2], "@")[0])
	initpath = append(initpath, fmt.Sprintf("%s-%s", parts[1], filename))

	out := strings.Join(initpath, "/")
	return out
}

func findMainclassInJar(jarfile string) (string, error) {
	data, err := util.ReadFileFromZip(jarfile, "META-INF/MANIFEST.MF")
	if err != nil {
		return "", err
	}
	for _, line := range strings.Split(string(data), "\n") {
		if strings.HasPrefix(line, "Main-Class:") {
			return strings.TrimSpace(strings.Split(line, " ")[1]), nil
		}
	}
	return "", nil
}
