/* vim:set ts=4 sw=4 noet nowrap syntax=go ff=unix:
 *
 * Copyright 2021 Mark Crewson <mark@crewson.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package modloader

import (
	"fmt"
	"os"
	"path/filepath"
	"gitlab.com/routhio/minecraft/tools/packgolib/util"
)

//

type ModLoader interface {
	GetLaunchArguments() (string, error)
	GetClientLibraries() ([]*util.Library, error)
	GetServerLibraries() ([]*util.Library, error)
	GetMainclass() (string, error)
	//GetServerJarFilename() (string, error)

	InstallClient(installLocation string, downloader *util.Downloader) error
	InstallServer(installLocation string, downloader *util.Downloader) error
}

//

func InstallModLoaderLibraries(libraries []*util.Library, downloader *util.Downloader, libraryLocation string) error {
	for _, library := range libraries {
		downloadFile := ""
		var err error
		for _, url := range library.GetDownloadUrls() {
			downloadFile, err = downloader.DownloadToSpecificFilename(url, library.GetPathLocal())
			if err == nil {
				break
			}

		}

		if downloadFile == "" {
			return fmt.Errorf("Cannot download library: {}", library.GetFilename())
		}

		cachedLib := filepath.Join(downloader.DownloadDir, library.GetPathLocal())
		localLib := filepath.Join(libraryLocation, library.GetPathLocal())
		if err := os.MkdirAll(filepath.Dir(localLib), 0777); err != nil {
			return err
		}
		if err := util.Copyfile(cachedLib, localLib); err != nil {
			return err
		}
	}
	return nil
}
