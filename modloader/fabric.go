/* vim:set ts=4 sw=4 noet nowrap syntax=go ff=unix:
 *
 * Copyright 2021 Mark Crewson <mark@crewson.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package modloader

import (
	"fmt"
	"path/filepath"
	"github.com/buger/jsonparser"
	"github.com/go-resty/resty/v2"

	"gitlab.com/routhio/minecraft/tools/packgolib/util"
)

const FABRIC_META_URL = "https://meta.fabricmc.net/v2/versions"
const FABRIC_MAVEN_URL = "https://maven.fabricmc.net/"

//

type FabricModLoader struct {
	minecraftVersion string
	loaderVersion    string
	manifest         []byte

	client *resty.Client
}

func NewFabricModLoader(minecraftVersion string, loaderVersion string) *FabricModLoader {
	f := &FabricModLoader{
		minecraftVersion: minecraftVersion,
		loaderVersion:    loaderVersion,
		client:           resty.New(),
	}
	return f
}

// ModLoader Interface

func (f *FabricModLoader) GetLaunchArguments() (string, error) {
	return "", nil
}

func (f *FabricModLoader) GetClientLibraries() ([]*util.Library, error) {
	return f.getLibraries("client")
}

func (f *FabricModLoader) GetServerLibraries() ([]*util.Library, error) {
	return f.getLibraries("server")
}

func (f *FabricModLoader) GetMainclass() (string, error) {
	manifest, err := f.getFabricManifest()
	if err != nil {
		return "", err
	}
	return jsonparser.GetString(manifest, "launcherMeta", "mainClass", "client")
}

func (f *FabricModLoader) GetServerJarFilename() (string, error) {
	return "fabric-server-launch.jar", nil
}

func (f *FabricModLoader) InstallClient(installLocation string, downloader *util.Downloader) error {
	libLocation := filepath.Join(installLocation, "libraries")
	libs, err := f.getLibraries("client")
	if err != nil {
		return err
	}
	return InstallModLoaderLibraries(libs, downloader.NewSubdownloader("libraries"), libLocation)
}

func (f *FabricModLoader) InstallServer(installLocation string, downloader *util.Downloader) error {
	return fmt.Errorf("not implemented yet")
}

// Internal functions

func (f *FabricModLoader) getLibraries(side string) ([]*util.Library, error) {
	manifest, err := f.getFabricManifest()
	if err != nil {
		return nil, err
	}

	var dependents []*util.Library
	loader, err := jsonparser.GetString(manifest, "loader", "maven")
	if err != nil {
		return nil, err
	}
	loaderLib, err := util.NewLibraryFromName(loader, FABRIC_MAVEN_URL)
	if err != nil {
		return nil, err
	}
	dependents = append(dependents, loaderLib)

	intermediary, err := jsonparser.GetString(manifest, "intermediary", "maven")
	if err != nil {
		return nil, err
	}
	intermediaryLib, err := util.NewLibraryFromName(intermediary, FABRIC_MAVEN_URL)
	if err != nil {
		return nil, err
	}
	dependents = append(dependents, intermediaryLib)

	_, err = jsonparser.ArrayEach(manifest, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		library, _ := util.NewLibrary(value)
		if library.Matches() {
			dependents = append(dependents, library)
		}
	}, "launcherMeta", "libraries", "common")
	if err != nil {
		return nil, err
	}

	jsonparser.ArrayEach(manifest, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		library, err := util.NewLibrary(value)
		if err != nil {
			if library.Matches() {
				dependents = append(dependents, library)
			}
		}
	}, "launcherMeta", "libraries", side)
	return dependents, nil
}

func (f *FabricModLoader) getFabricManifest() ([]byte, error) {
	if f.manifest == nil {
		if err := f.downloadFabricManifest(); err != nil {
			return nil, err
		}
	}
	return f.manifest, nil
}

func (f *FabricModLoader) downloadFabricManifest() error {
	manifestUrl := fmt.Sprintf("%s/loader/%s/%s", FABRIC_META_URL, f.minecraftVersion, f.loaderVersion)
	resp, err := f.client.R().Get(manifestUrl)
	if err != nil {
		return err
	}
	f.manifest = resp.Body()
	return nil
}
