/* vim:set ts=4 sw=4 noet nowrap syntax=go ff=unix:
 *
 * Copyright 2021 Mark Crewson <mark@crewson.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
)

type Downloader struct {
	DownloadDir string
}

func NewDownloader(downloadDir string) *Downloader {
	d := &Downloader{
		DownloadDir: downloadDir,
	}
	return d
}

func (d *Downloader) NewSubdownloader(downloadDir string) *Downloader {
	return NewDownloader(filepath.Join(d.DownloadDir, downloadDir))
}

func (d *Downloader) DownloadToSpecificFilename(downloadUrl string, filename string) (string, error) {
	localfile := filepath.Join(d.DownloadDir, filename)
	if err := d.doFileDownload(downloadUrl, localfile); err != nil {
		return "", err
	}
	return localfile, nil
}

func (d *Downloader) DownloadToDefaultFilename(downloadUrl string) (string, error) {
	parsedurl, err := url.Parse(downloadUrl)
	if err != nil {
		return "", err
	}
	localfile := filepath.Join(d.DownloadDir, filepath.Base(parsedurl.Path))
	if err := d.doFileDownload(downloadUrl, localfile); err != nil {
		return "", err
	}
	return localfile, nil
}

func (d *Downloader) doFileDownload(url string, destination string) error {
	if _, err := os.Stat(destination); err == nil {
		// cached
		return nil
	}
	destDir := filepath.Dir(destination)
	if err := os.MkdirAll(destDir, 0777); err != nil {
		return err
	}

	tempfile := filepath.Join(destDir, fmt.Sprintf(".downloading_%s", filepath.Base(destination)))
	if err := os.Remove(tempfile); err != nil && !os.IsNotExist(err) {
		return err
	}
	defer os.Remove(tempfile)

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Returned bad status: %s", resp.Status)
	}

	out, err := os.Create(tempfile)
	if err != nil {
		return err
	}
	defer out.Close()

	if _, err := io.Copy(out, resp.Body); err != nil {
		return err
	}

	return os.Rename(tempfile, destination)
}
