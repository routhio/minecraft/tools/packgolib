/* vim:set ts=4 sw=4 noet nowrap syntax=go ff=unix:
 *
 * Copyright 2021 Mark Crewson <mark@crewson.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util

import (
	"archive/tar"
	"archive/zip"
	"compress/bzip2"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

//

func Copyfile(src string, dest string) error {
	srcStat, err := os.Stat(src)
	if err != nil {
		return err
	}
	if !srcStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return err
	}
	defer source.Close()

	destination, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer destination.Close()

	_, err = io.Copy(destination, source)
	return err
}

//

func ReadFileFromZip(zipfile string, filename string) ([]byte, error) {
	zipf, err := zip.OpenReader(zipfile)
	if err != nil {
		return nil, err
	}
	defer zipf.Close()
	for _, f := range zipf.File {
		if f.Name != filename {
			continue
		}
		source, err := f.Open()
		if err != nil {
			return nil, err
		}
		defer source.Close()
		return ioutil.ReadAll(source)
	}
	return nil, fmt.Errorf("No '%s' found inside zip file: %s", filename, zipfile)
}

func ExtractFileFromZip(zipfile string, filename string, destination string) error {
	zipf, err := zip.OpenReader(zipfile)
	if err != nil {
		return err
	}
	defer zipf.Close()
	for _, f := range zipf.File {
		if f.Name != filename {
			continue
		}
		source, err := f.Open()
		if err != nil {
			return err
		}
		defer source.Close()
		dest, err := os.Create(destination)
		if err != nil {
			return err
		}
		defer dest.Close()
		if _, err = io.Copy(dest, source); err != nil {
			return err
		}
		return nil
	}
	return fmt.Errorf("No '%s' found inside zip file: %s", filename, zipfile)
}

//

func ReadFileFromTarBz2(tarfile string, filename string) ([]byte, error) {
	in, err := os.Open(tarfile)
	if err != nil {
		return nil, err
	}
	defer in.Close()
	tarf := tar.NewReader(bzip2.NewReader(in))
	for {
		header, err := tarf.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		if header.Name != filename {
			continue
		}
		if header.Typeflag != tar.TypeReg {
			continue
		}

		return ioutil.ReadAll(tarf)
	}
	return nil, fmt.Errorf("No '%s' found inside tbz2 file: %s", filename, tarfile)
}

func ExtractFileFromTarBz2(tarfile string, filename string, destination string) error {
	in, err := os.Open(tarfile)
	if err != nil {
		return err
	}
	defer in.Close()
	tarf := tar.NewReader(bzip2.NewReader(in))
	for {
		header, err := tarf.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		if header.Name != filename {
			continue
		}
		if header.Typeflag != tar.TypeReg {
			continue
		}

		dest, err := os.Create(destination)
		if err != nil {
			return err
		}
		defer dest.Close()
		if _, err = io.Copy(dest, tarf); err != nil {
			return err
		}
		return nil
	}
	return fmt.Errorf("No '%s' found inside tbz2 file: %s", filename, tarfile)
}

//

func MinecraftVersionAsInteger(version string) (int, error) {
	parts := strings.Split(version, ".")
	major, err := strconv.Atoi(parts[0])
	if err != nil {
		return 0, err
	}
	minor, err := strconv.Atoi(parts[1])
	if err != nil {
		return 0, err
	}
	micro, err := strconv.Atoi(parts[2])
	if err != nil {
		return 0, err
	}
	return (major * 10000) + (minor * 100) + micro, nil
}
