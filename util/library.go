/* vim:set ts=4 sw=4 noet nowrap syntax=go ff=unix:
 *
 * Copyright 2021 Mark Crewson <mark@crewson.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util

import (
	"fmt"
	"github.com/buger/jsonparser"
	"path/filepath"
	"runtime"
	"strings"
)

const CURSECDN_URL_BASE = "https://modloaders.cursecdn.com/647622546/maven/"

var GENERIC_FALLBACK_MAVEN_BASE_URLS = []string{"https://libraries.minecraft.net/",
	"https://files.minecraftforge.net/maven/",
	"https://repo1.maven.org/maven2/"}

//

type Library struct {
	Name                                         string
	json                                         []byte
	group, artifact, version, libtype, extension string
	native, arch                                 string
}

func NewLibraryFromName(name, url string) (*Library, error) {
	json := []byte(fmt.Sprintf("{ \"name\": \"%s\", \"url\": \"%s\" }", name, url))
	return NewLibrary(json)
}

func NewLibrary(libraryJson []byte) (*Library, error) {
	name, err := jsonparser.GetString(libraryJson, "name")
	if err != nil {
		return nil, err
	}
	parts := strings.Split(name, ":")
	group := parts[0]
	artifact := parts[1]
	version := parts[2]
	libtype := ""
	if len(parts) >= 4 {
		libtype = parts[3]
	}
	extension := "jar"
	if strings.Contains(version, "@") {
		parts = strings.SplitN(version, "@", 2)
		version = parts[0]
		extension = parts[1]
	}

	native := runtime.GOOS
	if native == "darwin" {
		native = "osx"
	}
	arch := "32"
	if runtime.GOARCH == "amd64" {
		arch = "64"
	}

	l := &Library{
		Name:      name,
		json:      libraryJson,
		group:     group,
		artifact:  artifact,
		version:   version,
		libtype:   libtype,
		extension: extension,
		native:    native,
		arch:      arch,
	}
	return l, nil
}

func (l *Library) GetNative() string {
	ns, err := jsonparser.GetString(l.json, "natives", l.native)
	if err == nil {
		return strings.ReplaceAll(ns, "${arch}", l.arch)
	}
	return ""
}

func (l *Library) Matches() bool {
	allowed := false
	_, err := jsonparser.ArrayEach(l.json, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		osName, err := jsonparser.GetString(value, "os", "name")
		if err == nil {
			if osName == l.native {
				action, _ := jsonparser.GetString(value, "action")
				if action == "allow" {
					allowed = true
				}
			}
		} else {
			action, _ := jsonparser.GetString(value, "action")
			if action == "allow" {
				allowed = true
			}
		}
	}, "rules")
	if err != nil {
		return true
	}
	return allowed
}

func (l *Library) GetPath() string {
	path, err := jsonparser.GetString(l.json, "path")
	if err == nil {
		return path
	}

	path, err = jsonparser.GetString(l.json, "downloads", "classifiers", l.GetNative(), "path")
	if err == nil {
		return path
	}

	path, err = jsonparser.GetString(l.json, "downloads", "artifact", "path")
	if err == nil {
		return path
	}

	return strings.Join([]string{strings.ReplaceAll(l.group, ".", "/"),
		l.artifact,
		l.version,
		l.GetFilename()}, "/")
}

func (l *Library) GetPathLocal() string {
	return filepath.FromSlash(l.GetPath())
}

func (l *Library) GetFilename() string {
	native := l.GetNative()
	if native != "" {
		return fmt.Sprintf("%s-%s-%s.%s", l.artifact, l.version, native, l.extension)
	}
	if l.libtype != "" {
		return fmt.Sprintf("%s-%s-%s.%s", l.artifact, l.version, l.libtype, l.extension)
	}
	return fmt.Sprintf("%s-%s.%s", l.artifact, l.version, l.extension)
}

func (l *Library) GetDownloadUrl() string {
	native := l.GetNative()
	if native != "" {
		url, err := jsonparser.GetString(l.json, "downloads", "classifiers", native, "url")
		if err == nil {
			return url
		}
	}
	url, err := jsonparser.GetString(l.json, "downloads", "artifact", "url")
	if err == nil {
		return url
	}
	url, err = jsonparser.GetString(l.json, "url")
	if err == nil {
		return fmt.Sprintf("%s%s", url, l.GetPath())
	}
	return ""
}

func (l *Library) getCursecdnDownloadUrl() string {
	native := l.GetNative()
	if native == "" {
		return CURSECDN_URL_BASE + l.GetPath()
	}
	return ""
}

func (l *Library) GetDownloadUrls() []string {
	var urls []string
	/* if curseUrl := l.getCursecdnDownloadUrl(); curseUrl != "" {
	    urls = append(urls, curseUrl)
	} */
	if refUrl := l.GetDownloadUrl(); refUrl != "" {
		urls = append(urls, refUrl)
	}
	for _, urlbase := range GENERIC_FALLBACK_MAVEN_BASE_URLS {
		urls = append(urls, urlbase+l.GetPath())
	}
	return urls
}

func (l *Library) MustExtract() bool {
	native := l.GetNative()
	if native == "" {
		return false
	}
	return true
}

func (l *Library) ExtractExcludes() []string {
	var excludes []string
	jsonparser.ArrayEach(l.json, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		excludes = append(excludes, string(value))
	}, "extract", "exclude")
	return excludes
}

func (l *Library) GetArtifact() string {
	return fmt.Sprintf("%s:%s", l.group, l.artifact)
}
