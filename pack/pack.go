/* vim:set ts=4 sw=4 noet nowrap syntax=go ff=unix:
 *
 * Copyright 2021 Mark Crewson <mark@crewson.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pack

import (
	"archive/tar"
	"compress/bzip2"
	"fmt"
	"github.com/buger/jsonparser"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/routhio/minecraft/tools/packgolib/minecraft"
	"gitlab.com/routhio/minecraft/tools/packgolib/modloader"
	"gitlab.com/routhio/minecraft/tools/packgolib/util"
)

type PackMetadata struct {
	Name    string
	Title   string
	Version string
	Authors []string
}

type PackManifest struct {
	MinecraftVersion string
	Modloaders       []*PackModloader
	Mods             []*PackResource
	ResourcePacks    []*PackResource
}

type PackModloader struct {
	Id      string
	Version string
}

type PackResource struct {
	Type        string
	Destination string
	Location    string
	Hash        string
	Size        int64
}

type PackFile struct {
	Name string
}

type Pack struct {
	Filename string
	Metadata *PackMetadata
	Manifest *PackManifest
	Files    []*PackFile
}

//

func ReadPack(filename string) (*Pack, error) {
	packErr := verifyPack(filename)
	if packErr != nil {
		return nil, packErr
	}

	metadata, metadataErr := extractMetadata(filename)
	if metadataErr != nil {
		return nil, metadataErr
	}

	manifest, manifestErr := extractManifest(filename)
	if manifestErr != nil {
		return nil, manifestErr
	}

	files, filesErr := extractFilesList(filename)
	if filesErr != nil {
		return nil, filesErr
	}

	p := &Pack{
		Filename: filename,
		Metadata: metadata,
		Manifest: manifest,
		Files:    files,
	}
	return p, nil
}

//

func verifyPack(filename string) error {
	pjson, err := util.ReadFileFromTarBz2(filename, "packmaker.json")
	if err != nil {
		return err
	}
	_, err = jsonparser.GetString(pjson, "packmaker")
	if err != nil {
		return err
	}
	_, err = jsonparser.GetString(pjson, "format_version")
	if err != nil {
		return err
	}
	return nil
}

func extractMetadata(filename string) (*PackMetadata, error) {
	metajson, err := util.ReadFileFromTarBz2(filename, "metadata.json")
	if err != nil {
		return nil, err
	}

	name, err := jsonparser.GetString(metajson, "name")
	if err != nil {
		return nil, err
	}
	title, err := jsonparser.GetString(metajson, "title")
	if err != nil {
		return nil, err
	}
	version, err := jsonparser.GetString(metajson, "version")
	if err != nil {
		return nil, err
	}

	var authors []string
	_, err = jsonparser.ArrayEach(metajson, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		authors = append(authors, string(value))
	}, "authors")
	if err != nil {
		return nil, err
	}

	pm := &PackMetadata{
		Name:    name,
		Title:   title,
		Version: version,
		Authors: authors,
	}
	return pm, nil
}

func extractManifest(filename string) (*PackManifest, error) {
	manifest, err := util.ReadFileFromTarBz2(filename, "manifest.json")
	if err != nil {
		return nil, err
	}

	mcVer, err := jsonparser.GetString(manifest, "minecraft", "version")
	if err != nil {
		return nil, err
	}

	var modloaders []*PackModloader
	_, err = jsonparser.ArrayEach(manifest, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		id, _ := jsonparser.GetString(value, "id")
		version, _ := jsonparser.GetString(value, "version")

		loader := &PackModloader{
			Id:      id,
			Version: version,
		}
		modloaders = append(modloaders, loader)
	}, "minecraft", "modloaders")

	var mods []*PackResource
	_, err = jsonparser.ArrayEach(manifest, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		rtype, _ := jsonparser.GetString(value, "type")
		destination, _ := jsonparser.GetString(value, "destination")
		location, _ := jsonparser.GetString(value, "location")
		hash, _ := jsonparser.GetString(value, "hash")
		size, _ := jsonparser.GetInt(value, "size")

		mod := &PackResource{
			Type:        rtype,
			Destination: destination,
			Location:    location,
			Hash:        hash,
			Size:        size,
		}
		mods = append(mods, mod)
	}, "mods")

	var resourcepacks []*PackResource
	_, err = jsonparser.ArrayEach(manifest, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		rtype, _ := jsonparser.GetString(value, "type")
		destination, _ := jsonparser.GetString(value, "destination")
		location, _ := jsonparser.GetString(value, "location")
		hash, _ := jsonparser.GetString(value, "hash")
		size, _ := jsonparser.GetInt(value, "size")

		rp := &PackResource{
			Type:        rtype,
			Destination: destination,
			Location:    location,
			Hash:        hash,
			Size:        size,
		}
		resourcepacks = append(resourcepacks, rp)
	}, "resourcepacks")

	pm := &PackManifest{
		MinecraftVersion: mcVer,
		Modloaders:       modloaders,
		Mods:             mods,
		ResourcePacks:    resourcepacks,
	}
	return pm, nil
}

func extractFilesList(filename string) ([]*PackFile, error) {
	var files []*PackFile

	in, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer in.Close()
	tarf := tar.NewReader(bzip2.NewReader(in))
	for {
		header, err := tarf.Next()
		if err == io.EOF {
			break
		}

		if header.Name == "packmaker.json" ||
			header.Name == "metadata.json" ||
			header.Name == "manifest.json" {
			continue
		}

		if header.Typeflag != tar.TypeReg {
			continue
		}

		file := &PackFile{
			Name: header.Name,
		}
		files = append(files, file)
	}
	return files, nil
}

//

func (p *Pack) Install(instancelocation string, downloader *util.Downloader) (*minecraft.Minecraft, modloader.ModLoader, error) {
	minecraft := minecraft.NewMinecraft(p.Manifest.MinecraftVersion)

	ml := p.Manifest.Modloaders[0]
	var modl modloader.ModLoader
	switch ml.Id {
	case "forge":
		modl = modloader.NewForgeModLoader(p.Manifest.MinecraftVersion, ml.Version, downloader)
	case "fabric":
		modl = modloader.NewFabricModLoader(p.Manifest.MinecraftVersion, ml.Version)
	}

	if err := minecraft.MakeClientInstance(downloader, instancelocation); err != nil {
		return nil, nil, err
	}

	if modl != nil {
		if err := modl.InstallClient(instancelocation, downloader); err != nil {
			return nil, nil, err
		}
	}

	fmt.Println("Installing mods ...")
	modDownloader := downloader.NewSubdownloader("mods")
	for _, mod := range p.Manifest.Mods {
		fmt.Printf("  Installing: %s\n", mod.Location)
		downloadFile, downloadErr := modDownloader.DownloadToDefaultFilename(mod.Location)
		if downloadErr != nil {
			return nil, nil, downloadErr
		}

		localMod := filepath.FromSlash(filepath.Join(instancelocation, mod.Destination))
		if err := os.MkdirAll(filepath.Dir(localMod), 0777); err != nil {
			return nil, nil, err
		}
		if err := util.Copyfile(downloadFile, localMod); err != nil {
			return nil, nil, err
		}
	}

	fmt.Println("Installing resourcepacks ...")
	rpDownloader := downloader.NewSubdownloader("resourcepacks")
	for _, rp := range p.Manifest.ResourcePacks {
		fmt.Printf("  Installing: %s\n", rp.Location)
		downloadFile, downloadErr := rpDownloader.DownloadToDefaultFilename(rp.Location)
		if downloadErr != nil {
			return nil, nil, downloadErr
		}

		localRp := filepath.FromSlash(filepath.Join(instancelocation, rp.Destination))
		if err := os.MkdirAll(filepath.Dir(localRp), 0777); err != nil {
			return nil, nil, err
		}
		if err := util.Copyfile(downloadFile, localRp); err != nil {
			return nil, nil, err
		}
	}

	fmt.Println("Installing pack files ...")

	input, inputerr := os.Open(p.Filename)
	if inputerr != nil {
		return nil, nil, inputerr
	}
	defer input.Close()
	tarf := tar.NewReader(bzip2.NewReader(input))
	for {
		header, tarferr := tarf.Next()
		if tarferr == io.EOF {
			break
		}
		if tarferr != nil {
			return nil, nil, tarferr
		}

		if header.Name == "packmaker.json" ||
			header.Name == "metadata.json" ||
			header.Name == "manifest.json" {
			continue
		}
		if header.Typeflag != tar.TypeReg {
			continue
		}

		src := header.Name
		fmt.Printf("  Installing: %s\n", src)

		destparts := strings.Split(src, "/")
		var dest string
		if destparts[0] == "files" {
			dest = filepath.Join(instancelocation, filepath.Join(destparts[1:]...))
		} else {
			dest = filepath.Join(instancelocation, src)
		}

		if err := os.MkdirAll(filepath.Dir(dest), 0777); err != nil {
			return nil, nil, err
		}
		destf, desterr := os.Create(dest)
		if desterr != nil {
			return nil, nil, desterr
		}
		defer destf.Close()
		if _, copyerr := io.Copy(destf, tarf); copyerr != nil {
			return nil, nil, copyerr
		}
	}

	return minecraft, modl, nil
}
