module gitlab.com/routhio/minecraft/tools/packgolib

go 1.14

require (
	github.com/buger/jsonparser v1.0.0
	github.com/go-resty/resty/v2 v2.3.0
)
