# PACKGOLIB

A golang library for handling Minecraft modpacks. Still unversioned and highly volatile.  So far it can:

* Install modern minecraft clients
* Install packmaker "pack" modpacks
* Launch the game in offline mod with the modpack content
* More to come...
