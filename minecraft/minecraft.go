/* vim:set ts=4 sw=4 noet nowrap syntax=go ff=unix:
 *
 * Copyright 2021 Mark Crewson <mark@crewson.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minecraft

import (
	"archive/zip"
	"errors"
	"fmt"
	"github.com/buger/jsonparser"
	"github.com/go-resty/resty/v2"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/routhio/minecraft/tools/packgolib/util"
)

const MINECRAFT_VERSION_MANIFEST_URL = "https://launchermeta.mojang.com/mc/game/version_manifest.json"
const MINECRAFT_ASSETS_BASE_URL = "http://resources.download.minecraft.net/"

//

type Minecraft struct {
	Version          string
	versionsManifest []byte
	manifest         []byte
	assetsIndex      []byte
	id               string

	client *resty.Client
}

func NewMinecraft(version string) *Minecraft {
	m := &Minecraft{
		Version: version,
		client:  resty.New(),
	}
	return m
}

// Make a client instance

func (m *Minecraft) MakeClientInstance(downloader *util.Downloader, buildlocation string) error {
	if err := m.DownloadClient(downloader); err != nil {
		return err
	}

	manifest, err := m.GetManifest()
	if err != nil {
		return err
	}
	id, err := m.GetId()
	if err != nil {
		return err
	}

	// Copy the minecraft client library

	cachedLib := filepath.Join(downloader.DownloadDir, "versions", id, "client.jar")
	localLib := filepath.Join(buildlocation, "libraries", "com", "mojang", "minecraft", id,
		fmt.Sprintf("minecraft-%s-client.jar", id))
	if err := os.MkdirAll(filepath.Dir(localLib), 0777); err != nil {
		return err
	}
	if err := util.Copyfile(cachedLib, localLib); err != nil {
		return err
	}

	// Copy the minecraft libraries
	libs, err := m.GetLibraries()
	if err != nil {
		return err
	}
	for _, lib := range libs {
		cachedLib = filepath.Join(downloader.DownloadDir, "libraries", lib.GetPathLocal())
		if lib.MustExtract() {
			extractedlib, err := zip.OpenReader(cachedLib)
			if err != nil {
				return err
			}
			defer extractedlib.Close()

			for _, f := range extractedlib.File {
				excluded := false
				if strings.HasSuffix(f.Name, "/") {
					// ignore directory entries
					excluded = true
				} else {
					for _, exclude := range lib.ExtractExcludes() {
						if strings.HasPrefix(f.Name, exclude) {
							excluded = true
							break
						}
					}
				}
				if !excluded {
					dest := filepath.Join(buildlocation, "natives", f.Name)
					if err := os.MkdirAll(filepath.Dir(dest), 0777); err != nil {
						return err
					}
					source, err := f.Open()
					if err != nil {
						return err
					}
					defer source.Close()
					destination, err := os.Create(dest)
					if err != nil {
						return err
					}
					defer destination.Close()
					if _, err = io.Copy(destination, source); err != nil {
						return err
					}
				}
			}
		} else {
			localLib = filepath.Join(buildlocation, "libraries", lib.GetPathLocal())
			if err := os.MkdirAll(filepath.Dir(localLib), 0777); err != nil {
				return err
			}
			if err := util.Copyfile(cachedLib, localLib); err != nil {
				return err
			}
		}
	}

	// Copy the minecraft assets

	assetIndex, err := m.GetAssetsIndex()
	if err != nil {
		return err
	}

	assetIndexDir := filepath.Join(buildlocation, "assets", "indexes")
	if err := os.MkdirAll(assetIndexDir, 0777); err != nil {
		return err
	}

	assets, err := jsonparser.GetString(manifest, "assets")
	if err != nil {
		return err
	}
	assetIndexFile := fmt.Sprintf("%s.json", assets)

	cachedAssetIndexFile := filepath.Join(downloader.DownloadDir, "assets", "indexes", assetIndexFile)
	localAssetIndexFile := filepath.Join(assetIndexDir, assetIndexFile)
	if err := util.Copyfile(cachedAssetIndexFile, localAssetIndexFile); err != nil {
		return err
	}

	jsonparser.ObjectEach(assetIndex, func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
		hash, err := jsonparser.GetString(value, "hash")
		if err == nil {
			path := filepath.Join(hash[0:2], hash)

			cachedAssetFile := filepath.Join(downloader.DownloadDir, "assets", "objects", path)
			localAssetFile := filepath.Join(buildlocation, "assets", "objects", path)
			if err := os.MkdirAll(filepath.Dir(localAssetFile), 0777); err != nil {
				return err
			}

			err = util.Copyfile(cachedAssetFile, localAssetFile)
		}
		return err
	}, "objects")

	return nil
}

// Download Client

func (m *Minecraft) DownloadClient(downloader *util.Downloader) error {
	fmt.Println("Downloading minecraft client ...")
	if err := m.downloadClientJar(downloader); err != nil {
		return err
	}
	fmt.Println("Downloading minecraft client libraries ...")
	if err := m.downloadLibraries(downloader); err != nil {
		return err
	}
	fmt.Println("Downloading minecraft client assets ...")
	if err := m.downloadAssets(downloader); err != nil {
		return err
	}
	return nil
}

func (m *Minecraft) DownloadServer(downloader *util.Downloader) error {
	fmt.Println("Downloading minecraft server ...")
	if err := m.downloadServerJar(downloader); err != nil {
		return err
	}
	fmt.Println("Downloading minecraft server libraries ...")
	if err := m.downloadLibraries(downloader); err != nil {
		return err
	}
	return nil
}

func (m *Minecraft) downloadClientJar(downloader *util.Downloader) error {
	manifest, err := m.GetManifest()
	if err != nil {
		return err
	}
	clientUrl, err := jsonparser.GetString(manifest, "downloads", "client", "url")
	if err != nil {
		return err
	}
	version, err := m.GetId()
	if err != nil {
		return err
	}

	sub := downloader.NewSubdownloader(filepath.Join("versions", version))
	_, err = sub.DownloadToSpecificFilename(clientUrl, "client.jar")
	return err
}

func (m *Minecraft) downloadServerJar(downloader *util.Downloader) error {
	manifest, err := m.GetManifest()
	if err != nil {
		return err
	}
	serverUrl, err := jsonparser.GetString(manifest, "downloads", "server", "url")
	if err != nil {
		return err
	}
	version, err := m.GetId()
	if err != nil {
		return err
	}

	sub := downloader.NewSubdownloader(filepath.Join("versions", version))
	_, err = sub.DownloadToSpecificFilename(serverUrl, "server.jar")
	return err
}

func (m *Minecraft) downloadLibraries(downloader *util.Downloader) error {
	manifest, err := m.GetManifest()
	if err != nil {
		return err
	}

	_, err = jsonparser.ArrayEach(manifest, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		library, err := util.NewLibrary(value)
		if err == nil {
			if library.Matches() {
				libraryUrl := library.GetDownloadUrl()
				if libraryUrl != "" {
					path := library.GetPath()

					sub := downloader.NewSubdownloader(filepath.Join("libraries", filepath.Dir(path)))
					_, err = sub.DownloadToDefaultFilename(libraryUrl)
				}
			}
		}
	}, "libraries")
	if err != nil {
		return err
	}
	return nil
}

func (m *Minecraft) downloadAssets(downloader *util.Downloader) error {
	manifest, err := m.GetManifest()
	if err != nil {
		return err
	}
	assetIndex, err := m.GetAssetsIndex()
	if err != nil {
		return err
	}

	assetIndexDir := filepath.Join(downloader.DownloadDir, "assets", "indexes")
	err = os.MkdirAll(assetIndexDir, 0777)
	if err != nil {
		return err
	}

	assets, err := jsonparser.GetString(manifest, "assets")
	if err != nil {
		return err
	}
	assetIndexFile := filepath.Join(assetIndexDir, fmt.Sprintf("%s.json", assets))
	err = ioutil.WriteFile(assetIndexFile, assetIndex, 0644)
	if err != nil {
		return err
	}

	jsonparser.ObjectEach(assetIndex, func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
		hash, err := jsonparser.GetString(value, "hash")
		if err == nil {
			path := filepath.Join(hash[0:2], hash)
			url := MINECRAFT_ASSETS_BASE_URL + path
			sub := downloader.NewSubdownloader(filepath.Join("assets", "objects", filepath.Dir(path)))
			sub.DownloadToDefaultFilename(url)
		}
		return err
	}, "objects")
	return nil
}

// Versions Manifest

func (m *Minecraft) GetVersionsManifest() ([]byte, error) {
	if m.versionsManifest == nil {
		err := m.downloadVersionsManifest()
		if err != nil {
			return nil, err
		}
	}
	return m.versionsManifest, nil
}

func (m *Minecraft) downloadVersionsManifest() error {
	resp, err := m.client.R().Get(MINECRAFT_VERSION_MANIFEST_URL)
	if err != nil {
		return err
	}
	m.versionsManifest = resp.Body()
	return nil
}

// Manifest

func (m *Minecraft) GetManifest() ([]byte, error) {
	if m.manifest == nil {
		err := m.downloadManifest()
		if err != nil {
			return nil, err
		}
	}
	return m.manifest, nil
}

func (m *Minecraft) downloadManifest() error {
	manifestUrl := ""
	versionsManifest, err := m.GetVersionsManifest()
	if err != nil {
		return err
	}
	jsonparser.ArrayEach(versionsManifest, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		id, err := jsonparser.GetString(value, "id")
		if err == nil {
			if id == m.Version {
				manifestUrl, err = jsonparser.GetString(value, "url")
			}
		}
	}, "versions")

	if manifestUrl == "" {
		return errors.New(fmt.Sprintf("Cannot find a minecraft manifest for version %s", m.Version))
	}

	resp, err := m.client.R().Get(manifestUrl)
	if err != nil {
		return err
	}
	m.manifest = resp.Body()
	return nil
}

func (m *Minecraft) GetId() (string, error) {
	if m.id != "" {
		return m.id, nil
	}

	manifest, err := m.GetManifest()
	if err != nil {
		return "", err
	}
	id, err := jsonparser.GetString(manifest, "id")
	if err != nil {
		return "", err
	}
	m.id = id
	return m.id, err
}

func (m *Minecraft) GetLibraries() ([]*util.Library, error) {
	manifest, err := m.GetManifest()
	if err != nil {
		return nil, err
	}

	var libs []*util.Library
	jsonparser.ArrayEach(manifest, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		library, err := util.NewLibrary(value)
		if err == nil {
			if library.Matches() {
				libs = append(libs, library)
			}
		}
	}, "libraries")
	return libs, nil
}

// AssetsIndex

func (m *Minecraft) GetAssetsIndex() ([]byte, error) {
	if m.assetsIndex == nil {
		err := m.downloadAssetsIndex()
		if err != nil {
			return nil, err
		}
	}
	return m.assetsIndex, nil
}

func (m *Minecraft) downloadAssetsIndex() error {
	manifest, err := m.GetManifest()
	if err != nil {
		return err
	}
	assetsUrl, err := jsonparser.GetString(manifest, "assetIndex", "url")
	if err != nil {
		return err
	}

	resp, err := m.client.R().Get(assetsUrl)
	if err != nil {
		return err
	}
	m.assetsIndex = resp.Body()
	return nil
}

// Launch Arguments

func (m *Minecraft) GetMainClass() (string, error) {
	manifest, err := m.GetManifest()
	if err != nil {
		return "", err
	}
	mainClass, err := jsonparser.GetString(manifest, "mainClass")
	if err != nil {
		return "", err
	}
	return mainClass, nil
}

func (m *Minecraft) GetLaunchArguments() (string, error) {
	manifest, err := m.GetManifest()
	if err != nil {
		return "", err
	}
	arguments := ""
	_, err = jsonparser.ArrayEach(manifest, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		if dataType == jsonparser.String {
			arguments = arguments + " " + string(value)
		}
	}, "arguments", "game")
	if err == nil {
		return arguments, nil
	}

	return jsonparser.GetString(manifest, "minecraftArguments")
}
